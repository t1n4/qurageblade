import java.io.*;
import java.util.*;

public class Main {
	public static void main(String args[]) throws IOException {
		DataInputStream d = new DataInputStream(System.in);
		String c;
		String list = new String();
		int num = 0;
		while ((c = d.readLine()) != null) {
			if(num == 0) {
				num = Integer.parseInt(c);
			} else {
				list = c;
			}
		}

		int lq = 0;
		int posi = list.indexOf("Q");
		while(posi != -1) {
			lq ++;
			posi = list.indexOf("Q", posi + 1);
		}
		int sq = num -lq;
		int skin = ((2 * sq) + (8 * lq)) / 5;
		int fang = ((10 * sq) + (2 * lq)) / 6;
		int sfMin = Math.min(skin, fang);
		int eye = lq + sq;

		System.out.println(Math.min(sfMin, eye));
	}
}